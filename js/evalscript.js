$(function() {
    /* Calculator object has the string that will be evaluted and some helper
        variables/funcions */
    var Calculator = function() {
        this.toEval = "";
        this.displaySelector = $(".display");
        this.reset = function() {
            this.toEval = "";
            this.displaySelector.text("0");
        }
    }

    var calc = new Calculator();

    $(".buttonNum, .buttonOp, #buttonZero, #decimal").click(function() {
        var value = $(this).text();
        if (calc.displaySelector.text() === "OPERATIONAL ERROR" || calc.displaySelector.text() === "0"){
            calc.displaySelector.text("");
            calc.toEval = "";
        }

        calc.toEval += value;
        /* not using .append() because it broke down the font */
        calc.displaySelector.text(calc.toEval);
    });

    $("#CLR").click(function() {
        calc.reset();

    });
    $(".buttonEqual").click(function() {
        /* evaluates user input and does the calculations,
        if an error occurs (e.g. user made the wrong input, warning is shown) */
        try {
            calc.toEval = eval(calc.toEval);
            calc.displaySelector.text(calc.toEval);

        } catch (e) {
            calc.displaySelector.text("OPERATIONAL ERROR");
        }
    });
});